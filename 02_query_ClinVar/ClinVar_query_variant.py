#! /usr/bin/env python


def extractRCV(entry, varID, rsID, gene):
    accession = entry['accession']
    clinSig = entry['clinical_significance']
    clinName = entry['conditions']['name']
    omim = ""
    if 'omim' in entry['conditions']['identifiers']:
        omim = entry['conditions']['identifiers']['omim']

    synon = ""
    if 'synonyms' in entry['conditions']:
        synon = entry['conditions']['synonyms']

    dict_entry = {'Gene': gene, 'hgvs_ID': varID, 'rsID': rsID, 
                    'Clinical_Significance': clinSig, 
                    'Condition/Phenotype': clinName, 
                    'Clinical_synonyms': synon, 'OMIM_ID': omim,
                    'ClinVar_accession': accession }
    return {accession: dict_entry}


def parseRCV(currVar):
    varID = currVar._id
    rsID = currVar.dbsnp['rsid']
    rcv_list = currVar.clinvar['rcv']
    gene = currVar.clinvar['gene']['symbol']

    var_list = []
    if type(rcv_list) is list:
        for i in range(0, len(rcv_list)):
            x = extractRCV(rcv_list[i], varID, rsID, gene)
            var_list.append(x)
    else:
        x = extractRCV(rcv_list, varID, rsID, gene)
        var_list.append(x)
    return var_list


