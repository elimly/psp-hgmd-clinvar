#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/02_query_ClinVar

PSP_VARID_FILE=$BASE_PATH/ClinVar_HGMD_PSP_analysis/clinvar/PSP_extremely_rare/PSP_extremely_rare_recurring_varIDs.txt

DATA_NAME="PSP_extremely_rare_recurring"

python $SRC_PATH/ClinVar_query_variant_list.py -p $PSP_VARID_FILE  -d $DATA_NAME

