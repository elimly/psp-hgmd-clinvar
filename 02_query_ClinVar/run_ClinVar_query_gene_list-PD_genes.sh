#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/02_query_ClinVar

GENE_LIST=$BASE_PATH/PD_genes.txt
OUT_NAME="ClinVar_PD_genes-LRRK2_020317"
OUT_DIR=$BASE_PATH


python $SRC_PATH/ClinVar_query_gene_list.py -g $GENE_LIST --name $OUT_NAME --dir $OUT_DIR --write 

