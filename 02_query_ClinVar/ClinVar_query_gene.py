#! /usr/bin/env python
from datetime import datetime
import re, argparse, time, os
import pandas as pd
pd.options.mode.chained_assignment = None

## ClinVar query API - myvariant - setup
import myvariant
mv = myvariant.MyVariantInfo()


## specify ClinVar query fields 
query_fields = ['clinvar.gene.symbol', 'clinvar.rcv.clinical_significance',
				'clinvar.rcv.conditions.name', 'clinvar.rcv.conditions.synonyms', 
				'clinvar.rcv.conditions.identifiers.omim', 'clinvar.rcv.accession', 
				'clinvar.rcv.last_evaluated', 'clinvar.rcv.preferred_name', 
				'dbsnp.rsid', 'dbsnp.validated', 'chrom', 'hg19.start', 
				'_id', 'clinvar.ref', 'clinvar.alt', 'evs.hgvs']

## specify output columns
cols_clinvar = ['Gene', 'var_ID', 'rsID', 'ClinVar_Significance', 'ClinVar_disease',
				'ClinVar_synonyms', 'OMIM_ID', 'ClinVar_accession', 'ClinVar_date', 
				'ClinVar_name', 'dbSNP_Validated', 'Chr', 'Start', 'variant', 
				'var_coord', 'REF', 'ALT', 'hgvs_coding', 'hgvs_protein']



def getGeneQueryHits(gene):
	q_str = str(gene) + " AND _exists_:clinvar AND clinvar.type:\"single nucleotide variant\""
	query = mv.query(q=q_str, fields='_id', size=1000)
	q_hits = query['hits']
	if len(q_hits) > 0:
		print(str(gene), ": ", len(q_hits), " variant(s) found")
	return q_hits


def getVariants(queryList, queryFields=query_fields):
	varList = [mv.getvariant(q['_id'], fields=queryFields) for q in queryList]
	return varList


def extractRCV(entry, varID, rsID, gene, chrm, start, valid, ref, alt, cDNA, AA):
	accession = entry['accession']
	clinvar_clinSig = entry['clinical_significance']
	varChange = str(ref) + ">" + str(alt)
	position = chrm + ":" + str(start)
	eval_date = "not provided"
	var_name = entry['preferred_name']
	omim = " "
	synon = " "

	if 'last_evaluated' in entry:
		eval_date = entry['last_evaluated']

	if type(entry['conditions']) is list:
		clinName, synon, omim = [], [], []

		for i in range(0, len(entry['conditions'])):
			clinName.append(entry['conditions'][i]['name'])
			if 'identifiers' in entry['conditions'][i]:
				if 'omim' in entry['conditions'][i]['identifiers']:
					omim.append(entry['conditions'][i]['identifiers']['omim'])
			if 'synonyms' in entry['conditions'][i]:
				synon.append(entry['conditions'][i]['synonyms'])
	else:
		clinName = entry['conditions']['name']
		if 'identifiers' in entry['conditions']:
			if 'omim' in entry['conditions']['identifiers']:
				omim = entry['conditions']['identifiers']['omim']
		if 'synonyms' in entry['conditions']:
			synon = ('; '.join(entry['conditions']['synonyms']))

	dict_entry = {'Gene': gene, 'var_ID': varID, 'rsID': rsID,
					'ClinVar_Significance': clinvar_clinSig, 
					'ClinVar_disease': clinName, 
					'ClinVar_synonyms': synon, 'OMIM_ID': omim,
					'ClinVar_accession': accession, 'ClinVar_date': eval_date, 
					'Chr': chrm, 'Start': start, 
					'dbSNP_Validated': valid, 'ClinVar_name': var_name, 
					'var_coord': position, 'variant': varChange, 
					'REF': ref, 'ALT': alt, 'hgvs_coding': cDNA, 'hgvs_protein': AA}
	return {accession: dict_entry}


def parseVariant(currVar, currGene):
	var_list = []
	varID = currVar['_id']
	chrm = "chr" + str(currVar['chrom'])
	rcv_list = currVar['clinvar']['rcv']
	ref = currVar['clinvar']['ref']
	alt = currVar['clinvar']['alt']
	cDNA = " "
	AA = " "

	if 'evs' in currVar:
		if 'hgvs' in currVar['evs']:
			if 'coding' in currVar['evs']['hgvs']:
				cDNA = currVar['evs']['hgvs']['coding']
			if 'protein' in currVar['evs']['hgvs']:
				AA = currVar['evs']['hgvs']['protein']

	if 'hg19' in currVar:
		start = currVar['hg19']['start']
	else:
		x = currVar['_id'].split('.')[1]
		start = re.sub(r'\D', '', x)

	if 'dbsnp' in currVar:
		dbsnp = currVar['dbsnp']
		rsID = dbsnp['rsid']
		valid = dbsnp['validated']
	else:
		rsID = "none"
		valid = "n/a"

	if type(rcv_list) is list:
		temp, dates = [], []
		
		for i in range(0, len(rcv_list)):
			x = extractRCV(rcv_list[i], varID, rsID, currGene, chrm, start, valid, ref, alt, cDNA, AA)
			curr_date = x[rcv_list[i]['accession']]['ClinVar_date']
			if "not provided" in curr_date:
				var_list.append(x)
				return var_list
			temp.append(x)
			date = datetime.strptime(str(curr_date), "%Y-%m-%d").date()
			dates.append(date)

		val, idx = max((val, idx) for (idx, val) in enumerate(dates))
		var_list.append(temp[idx])

	else:
		x = extractRCV(rcv_list, varID, rsID, currGene, chrm, start, valid, ref, alt, cDNA, AA)
		var_list.append(x)

	return var_list


def parse_gene_variants(geneVarList, currGene, colsOutput=cols_clinvar):
	## parse variants
	v_list = [curr for v in geneVarList for curr in parseVariant(v, currGene)]
	
	## convert to Pandas DataFrame
	frames_list = [pd.DataFrame.from_dict(l, orient='index') for l in v_list]
	list_DF = pd.concat(frames_list)
	
	## select subset of columns to output
	list_DF = list_DF[colsOutput]
	
	return list_DF


def queryGene(gene, write=False, dir=None):
	vars_in_gene_list = getGeneQueryHits(gene)
	var_full_info_list = getVariants(vars_in_gene_list)
	
	if (len(vars_in_gene_list) > 0):
		varDF = parse_gene_variants(var_full_info_list, gene)
		
		if write:
			curr_date = time.strftime("%m%d%y")
			outFileName = gene + "_ClinVar_variants_all_" + curr_date + ".xlsx"
			if dir is not None:
				outFileName = os.path.join(dir, outFileName)
			
			varDF.to_excel(outFileName, sheet_name='Orig', index=False, header=True)
			
		return varDF

	return None




if __name__ == "__main__":
	print("Started python")

	parser = argparse.ArgumentParser()
	parser.add_argument("-g", "--gene",
						dest="gene", type=str, action='store',
						help="enter gene symbol to query")
	parser.add_argument("-w", "--write",
						dest="write_output", action='store_true', default=False,
						help="use option '-o' to save query to output file")
	parser.add_argument("-d", "--dir",
						dest="dir_out", type=str, action='store', default=None,
						help="use option '-d' to enter directory for output file")
	
	pargs = parser.parse_args()
	gene = pargs.gene
	writeOut = pargs.write_output
	directory = pargs.dir_out
	
	queryGene(gene, writeOut, directory)
	
	print("Finished")


