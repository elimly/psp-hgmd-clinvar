#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/02_query_ClinVar

GENE_LIST=$BASE_PATH/PSP_rare_exclusive/PSP_rare_exclusive_geneList.txt
OUT_NAME="ClinVar_PSP_rare-exclusive_021317"
OUT_DIR=$BASE_PATH/PSP_rare_exclusive


python $SRC_PATH/ClinVar_query_gene_list.py -g $GENE_LIST --name $OUT_NAME --dir $OUT_DIR --write 

