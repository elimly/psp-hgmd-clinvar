#! /usr/bin/env python
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## ClinVar query API - myvariant - setup
import myvariant
mv = myvariant.MyVariantInfo()

## custom Python module written using the myvariant API
import ClinVar_query_variant


## specify input file column names
cols_orig = ['Variant', 'REF', 'ALT', 'Gene', 'hgvs_ID', 'variant_pos', 'rsID']

## specify ClinVar query columns
cols_clinvar = ['Gene', 'hgvs_ID', 'rsID', 'Clinical_Significance', 
				'Condition/Phenotype', 'Clinical_synonyms', 
				'OMIM_ID', 'ClinVar_accession']

## specify output columns
cols_merge = ['Variant', 'REF', 'ALT', 'Gene', 'hgvs_ID', 'rsID', 
				'Clinical_Significance', 'Condition/Phenotype', 
				'Clinical_synonyms', 'OMIM_ID', 'ClinVar_accession']



def processVariantList(variantDF):
	notInClinVar = variantDF[['Variant', 'REF', 'ALT', 'Gene', 'hgvs_ID', 'rsID']]
	notInClinVar.index = notInClinVar['Variant']
	notInClinVar['ClinVar'] = ""

	Variant_list = []

	for index, row in variantDF.iterrows():
		currRSID = str(row['rsID'])
		currVar = str(row['Variant'])
		if currRSID == "none":
			print(currRSID, " no rsID")
			notInClinVar.loc[currVar, "ClinVar"] = "not reported"
		else:
			hgvs = str(row['hgvs_ID'])
			check_var = mv.query(q=currRSID)['hits'][0]
			check_var_h = mv.getvariant(hgvs)

			if 'clinvar' in check_var_h:
				# var = myvariant.Variant.get(hgvs)
				var = mv.getvariant(hgvs)
				Variant_list.append(var)
				print(hgvs, " found in getVariant")
			elif 'clinvar'  in check_var:
				# var = myvariant.Variant.find_by(q=currRSID)[0]
				var = mv.query(currRSID)['hits'][0]
				Variant_list.append(var)
				print(currRSID, " found in query")
			else:
				notInClinVar.loc[currVar, "ClinVar"] = "not reported"

	return (notInClinVar, Variant_list)



def getClinVarAnnotations(variant_list):
	v_list = []
	for v in variant_list:
		curr = ClinVar_query_variant.parseRCV(v)
		v_list.extend(curr)

	frames_list = []
	for l in v_list:
		frames_list.append(pd.DataFrame.from_dict(l, orient='index'))
	list_DF = pd.concat(frames_list)
	list_DF = list_DF[cols_clinvar]
	return list_DF


def runProgram(varFile, dataName, inputCols=cols_orig, cvCols=cols_clinvar, outCols=cols_merge):
	all_var_DF = pd.read_table(varFile)
	all_var_DF.columns = inputCols

	processed =  processVariantList(all_var_DF)
	clinvarQuery_DF = processed[0]
	var_list = processed[1]

	clinvarQuery_DF.index = clinvarQuery_DF['Variant']
	notReported_DF = clinvarQuery_DF[clinvarQuery_DF['ClinVar'] == "not reported"]

	annot_DF = getClinVarAnnotations(var_list)
	annot_DF.columns = cvCols

	inClinVar = annot_DF['hgvs_ID'].drop_duplicates().tolist()
	extract_DF = clinvarQuery_DF[['Variant', 'REF', 'ALT', 'hgvs_ID']]
	extract_DF = extract_DF[extract_DF['hgvs_ID'].isin(inClinVar)]

	clinVar_DF = pd.merge(extract_DF, annot_DF, how="left", left_on=['hgvs_ID'], right_on=['hgvs_ID'])
	clinVar_DF = clinVar_DF[outCols]

	for index, row in clinVar_DF.iterrows():
		currVar = str(row['Variant'])
		clinSig = str(row['Clinical_Significance'])
		clinSig_str = "ClinVar: " + clinSig

		currSig = str(clinvarQuery_DF.loc[currVar, 'ClinVar'])
		if len(currSig) == 0:
			clinvarQuery_DF.loc[currVar, 'ClinVar'] = clinSig_str

	clinvarQuery_DF.to_excel(dataName + "_ClinVar_query.xlsx", sheet_name= 'Orig', index=False, header=True)
	clinVar_DF.to_csv(dataName + "_ClinVar_details.txt", sep='\t', index=False, header=True)
	notReported_DF.to_csv(dataName + "_notInClinVar.txt", sep='\t', index=False, header=True)

	return (clinvarQuery_DF, clinVar_DF, notReported_DF)



## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started python")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--psp_varID",
						dest="psp_varID_file", type=valid_file, action='store',
						help="path for the PSP varID file")
	parser.add_argument("-d", "--dataset",
						dest="dataset_name", type=str, action='store',
						help="Name for the current dataset")
	
	pargs = parser.parse_args()
	varFile = pargs.psp_varID_file
	dataName = pargs.dataset_name
	
	print("data name: \t", dataName)
	runProgram(varFile, dataName)
	
	print("Exiting Python program. Goodbye.")
	



