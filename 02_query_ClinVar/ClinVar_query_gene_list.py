#! /usr/bin/env python
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## custom Python module written using the myvariant API
import ClinVar_query_gene



def queryGeneList(genes, file_name="", write=False, dir=None):
	## variable set up
	df_list, not_found = [], []
	all_variants_DF, notFound_DF = None, None
	
	## query gene list
	for g in genes:
		curr = ClinVar_query_gene.queryGene(g, False)
		if curr is not None:
			df_list.append(curr)
		else:
			not_found.append(g)
	
	## concatenate DFs
	if len(df_list) > 0:
		all_variants_DF = pd.concat(df_list)
		print("\n# of variants found = \t", all_variants_DF.shape[0])
	
	if len(not_found) > 0:
		notFound_DF = pd.DataFrame(not_found)
		print("\n# of genes with NO reported variants = \t", notFound_DF.shape[0])
	
	## write output files
	if write:
		## set up output file names & paths
		out_file = file_name + '.xlsx'
		not_found_file = file_name + '_not-Found.xlsx'
		if dir is not None:
			out_file = os.path.join(dir, out_file)
			not_found_file = os.path.join(dir, not_found_file)
		
		if all_variants_DF:
			all_variants_DF.to_excel(out_file, sheet_name='Orig', index=False, header=True)
		
		if notFound_DF:
			notFound_DF.to_excel(not_found_file, sheet_name='Orig', index=False, header=True)
	
	return {'clinvar':all_variants_DF, 'not_found':notFound_DF}



## helper fxn
def loadGeneListFile(geneListFile):
	genes = [line.rstrip('\n') for line in open(geneListFile)]
	return genes



def runProgram(gene_list_file, file_name, write, dir):
	## load input gene list file
	genes = loadGeneListFile(gene_list_file)
	
	## query gene list
	queryGeneList(genes, file_name, write, dir)




## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started python")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-g", "--genes",
						dest="gene_list_file", type=valid_file, action='store',
						help="path for the gene list file")
	parser.add_argument("-n", "--name",
						dest="file_name", type=str, action='store',
						help="Name for the resulting ClinVar query output file")
	parser.add_argument("-w", "--write",
						dest="write_output", action='store_true', default=True,
						help="True: write output file, False: do NOT write")
	parser.add_argument("-d", "--dir",
						dest="dir_out", type=str, action='store', default=None,
						help="use option '-d' to enter directory for output file")
	
	pargs = parser.parse_args()
	gene_list_file = pargs.gene_list_file
	file_name = pargs.file_name
	writeOut = pargs.write_output
	dir = pargs.dir_out
	
	runProgram(gene_list_file, file_name, writeOut, dir)
	
	print("Finished")

