#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PSP_rare_exclusive
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/05_compare_PSP-ClinVar-HGMD

PSP_VAR_FILE=$BASE_PATH/PSP_exclusive_recurring_1021-varID.xlsx
CLINICAL_VAR_FILE=$BASE_PATH/PSP_rare-exclusive_021617_HGMD_ClinVar_variants.xlsx
OUT_DIR=$BASE_PATH/clinical
DATA_NAME="PSP_exclusive"
RARE_TYPE="exclusive"


python $SRC_PATH/compare_PSP_ClinVar_HGMD_rare-exclusive.py -p $PSP_VAR_FILE -c $CLINICAL_VAR_FILE -n $DATA_NAME -o $OUT_DIR --rare_type $RARE_TYPE

