#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/05_compare_PSP-ClinVar-HGMD

PSP_VAR_FILE=$BASE_PATH/PSP_variants_PD_genes-LRRK2_012717.xlsx
CLINICAL_VAR_FILE=$BASE_PATH/PD_genes-LRRK2_020317_HGMD_ClinVar_variants.xlsx
OUT_DIR=$BASE_PATH/clinical
DATA_NAME="PSP_PD_genes"


python $SRC_PATH/compare_PSP_ClinVar_HGMD.py -p $PSP_VAR_FILE -c $CLINICAL_VAR_FILE -n $DATA_NAME -o $OUT_DIR

