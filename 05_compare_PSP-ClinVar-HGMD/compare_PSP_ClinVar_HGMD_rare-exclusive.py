#! /usr/bin/env python

import time
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## myvariant API setup
import myvariant
mv = myvariant.MyVariantInfo()


def compareVariants(psp_df, reported_df):
	## ID EXACT variants found in both:
	psp_varID_list = psp_df['var_ID'].tolist()
	known_varID_list = reported_df['var_ID'].tolist()
	varIDs_in_both = list(set(psp_varID_list).intersection(set(known_varID_list)))
	
	psp_exact_DF = psp_df[psp_df['var_ID'].isin(varIDs_in_both)]
	known_exact_DF = reported_df[reported_df['var_ID'].isin(varIDs_in_both)]

	psp_notRported_DF = psp_df[~psp_df['var_ID'].isin(varIDs_in_both)]
	# known_nonexact_DF = reported_df[~reported_df['var_ID'].isin(varIDs_in_both)]


	psp_varCoord_list = psp_notRported_DF['VAR'].tolist()
	known_varCoord_list = reported_df['var_coord'].tolist()
	varCoord_in_both = list(set(psp_varCoord_list).intersection(set(known_varCoord_list)))

	psp_coord_DF = psp_notRported_DF[psp_notRported_DF['VAR'].isin(varCoord_in_both)]
	known_coord_DF = reported_df[reported_df['var_coord'].isin(varCoord_in_both)]
	
	print("# varIDs in both: ", len(varIDs_in_both))
	print("PSP exact: ", psp_exact_DF.shape[0])
	print("known exact: ", known_exact_DF.shape[0])
	
	print("\n\n\nPSP:")
	print("PSP total:", psp_df.shape[0])
	print("PSP variants reported (exact variant): ", psp_exact_DF.shape[0])
	print("PSP variants NOT reported: ", psp_notRported_DF.shape[0])
	print("PSP (unreported) variants at reported sites: ", psp_coord_DF.shape[0])

	print("\n\n\nHGMD / ClinVar:")
	print("HGMD / ClinVar total:", reported_df.shape[0])
	print("HGMD / ClinVar exact: ", known_exact_DF.shape[0])
	print("HGMD / ClinVar position: ", known_coord_DF.shape[0])

	return (psp_exact_DF, known_exact_DF, psp_coord_DF, known_coord_DF)



def outputReportedVariants(psp_exact, known_exact, data_name, file_ext, out_dir, rare_type):
	if "exclusive" in rare_type:
		select_cols = ['Gene', 'var_ID', 'H_Variant_class', 'H_Disease', 'C_Significance', 'C_disease',
					   'MAC', 'MAF',  'EXAC MAF', 'H_sift_score',
					   'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE',
					   'dbSNP', 'dbSNP_Validated','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
					   'H_accession', 'C_accession', 'H_HGVS_protein', 'C_HGVS_protein']
		select_cols_rename = ['Gene', 'var_ID', 'H_Variant_class', 'H_Disease', 'C_Significance',
					   'C_disease', 'MAC', 'MAF',  'EXAC MAF', 'H_sift_score',
					   'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
					   'dbSNP', 'dbSNP_Validated', 'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
					   'H_accession', 'C_accession', 'H_HGVS_protein', 'C_HGVS_protein']
	
		psp_cols_to_add = ['var_ID', 'MAC', 'MAF', 'EXAC MAF']
		psp_cols_rename = ['var_ID', 'MAC', 'MAF', 'EXAC MAF']
	
	else:
		select_cols = ['Gene', 'var_ID', 'H_Variant_class', 'H_Disease', 'C_Significance', 'C_disease',
		               'MAC', 'MAF', 'MAF_ADSP', 'EXAC MAF', 'H_sift_score',
		               'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE',
		               'dbSNP', 'dbSNP_Validated','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
		               'H_accession', 'C_accession', 'H_HGVS_protein', 'C_HGVS_protein']
		select_cols_rename = ['Gene', 'var_ID', 'H_Variant_class', 'H_Disease', 'C_Significance',
		               'C_disease', 'MAC', 'MAF', 'ADSP MAF', 'EXAC MAF', 'H_sift_score',
		               'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
		               'dbSNP', 'dbSNP_Validated', 'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
		               'H_accession', 'C_accession', 'H_HGVS_protein', 'C_HGVS_protein']

		psp_cols_to_add = ['var_ID', 'MAC', 'MAF', 'MAF_ADSP', 'EXAC MAF']
		psp_cols_rename = ['var_ID', 'MAC', 'MAF', 'ADSP MAF', 'EXAC MAF']

	known_cols_to_add = ['Gene', 'var_ID', 'H_Variant_class', 'H_Disease', 'C_Significance',
						 'C_disease', 'H_Type', 'H_sift_score', 'H_sift_prediction',
						 'H_mutpred_score', 'H_PMID', 'H_Citation', 'C_OMIM', 'C_accession',
						 'H_accession', 'DB_reported', 'dbSNP', 'dbSNP_Validated', 'order', 'H_HGVS_protein', 'C_HGVS_protein']

	psp_exact_cols_to_add = psp_exact[psp_cols_to_add]
	psp_exact_cols_to_add.columns = psp_cols_rename
	known_exact_cols_to_add = known_exact[known_cols_to_add]
	
	psp_exact_annot_DF = pd.merge(psp_exact, known_exact_cols_to_add, how="left", on='var_ID')
	psp_exact_annot_DF.sort_values(by=['Gene', 'order', 'POS'], ascending=[True, True, True], inplace=True)
	psp_exact_annot_DF.drop(['order'], axis=1, inplace=True)

	known_exact_annot_DF = pd.merge(known_exact, psp_exact_cols_to_add, how="left", on='var_ID')
	known_exact_annot_DF.sort_values(by=['Gene', 'order', 'Start'], ascending=[True, True, True], inplace=True)
	known_exact_annot_DF.drop(['order'], axis=1, inplace=True)
	
	psp_subtable = psp_exact_annot_DF[select_cols]
	psp_subtable.columns = select_cols_rename

	table_file = os.path.join(out_dir, data_name + "_Reported" + file_ext)
	psp_file = os.path.join(out_dir, data_name + "_Reported_fullPSP" + file_ext)
	hgmdCV_file = os.path.join(out_dir, data_name + "_Reported_HGMD-CV" + file_ext)

	psp_subtable.to_excel(table_file, sheet_name='ALL', index=False, header=True)
	psp_exact_annot_DF.to_excel(psp_file, sheet_name='ALL', index=False, header=True)
	known_exact_annot_DF.to_excel(hgmdCV_file, sheet_name='ALL', index=False, header=True)




def outputKnownPositionVariants(psp_coord, known_coord, data_name, file_ext, out_dir, rare_type):
	if "exclusive" in rare_type:
		select_cols2 = ['Gene', 'VAR', 'REF', 'ALT', 'H_REF', 'H_ALT', 'MAC', 'MAF',
						 'EXAC MAF', 'H_Variant_class', 'H_Disease',
						'C_Significance', 'C_disease',  'H_sift_score', 'H_sift_prediction',
						'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE', 'rsID', 'dbSNP',
						'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported', 'H_accession', 'C_accession',
						'var_ID', 'H_var_ID', 'H_HGVS_protein', 'C_HGVS_protein']
		select_cols2_rename = ['Gene', 'PSP_var_coord', 'PSP REF', 'PSP ALT', 'H_REF', 'H_ALT', 'MAC',
							   'MAF', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
							   'C_Significance', 'C_disease',  'H_sift_score',
							   'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
							   'PSP_rsID', 'H_rsID','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
							   'H_accession','C_accession', 'PSP_var_ID', 'H_var_ID', 'H_HGVS_protein', 'C_HGVS_protein']
	
		psp_cols_to_add2 = ['VAR', 'var_ID', 'REF', 'ALT', 'MAC', 'MAF',
							'EXAC MAF', 'rsID', 'SNPEFF_AMINO_ACID_CHANGE']
		psp_cols_rename = ['PSP_var_coord', 'PSP_var_ID', 'PSP_REF', 'PSP_ALT', 'MAC', 'MAF',
							'EXAC MAF', 'PSP_rsID', 'Protein change']

	else:
		select_cols2 = ['Gene', 'VAR', 'REF', 'ALT', 'H_REF', 'H_ALT', 'MAC', 'MAF', 'MAF_ADSP',
		                 'EXAC MAF', 'H_Variant_class', 'H_Disease',
		                'C_Significance', 'C_disease', 'H_sift_score', 'H_sift_prediction',
		                'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE', 'rsID', 'dbSNP',
		                'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported', 'H_accession', 'C_accession',
		                'var_ID', 'H_var_ID', 'H_HGVS_protein', 'C_HGVS_protein']
		select_cols2_rename = ['Gene', 'PSP_var_coord', 'PSP REF', 'PSP ALT', 'H_REF', 'H_ALT', 'MAC',
		                       'MAF', 'ADSP MAF', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
		                       'C_Significance', 'C_disease', 'H_sift_score',
		                       'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
		                       'PSP_rsID', 'H_rsID','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
		                       'H_accession','C_accession', 'PSP_var_ID', 'H_var_ID', 'H_HGVS_protein', 'C_HGVS_protein']
	
		psp_cols_to_add2 = ['VAR', 'var_ID', 'REF', 'ALT', 'MAC', 'MAF', 'MAF_ADSP',
		                    'EXAC MAF', 'rsID', 'SNPEFF_AMINO_ACID_CHANGE']
		psp_cols_rename = ['PSP_var_coord', 'PSP_var_ID', 'PSP_REF', 'PSP_ALT', 'MAC', 'MAF',
		                   'ADSP MAF', 'EXAC MAF', 'PSP_rsID', 'Protein change']

	known_cols_to_add2 = ['Gene', 'var_coord', 'var_ID', 'H_REF', 'H_ALT', 'H_Variant_class',
						  'H_Disease', 'C_Significance', 'C_disease', 'H_Type', 'H_sift_score',
						  'H_sift_prediction', 'H_mutpred_score', 'H_PMID', 'H_Citation', 'C_OMIM',
						  'C_accession', 'H_accession', 'DB_reported', 'dbSNP', 'dbSNP_Validated',
						  'order', 'H_HGVS_protein', 'C_HGVS_protein']
	known_cols_rename = ['Gene', 'H_var_coord', 'H_var_ID', 'H_REF', 'H_ALT', 'H_Variant_class',
						 'H_Disease', 'C_Significance', 'C_disease', 'H_Type', 'H_sift_score',
						 'H_sift_prediction', 'H_mutpred_score', 'H_PMID', 'H_Citation', 'C_OMIM',
						 'C_accession', 'H_accession', 'DB_reported', 'dbSNP', 'dbSNP_Validated',
						 'order', 'H_HGVS_protein', 'C_HGVS_protein']

	psp_coord_cols_to_add = psp_coord[psp_cols_to_add2]
	psp_coord_cols_to_add.columns = psp_cols_rename
	known_coord_cols_to_add = known_coord[known_cols_to_add2]
	known_coord_cols_to_add.columns = known_cols_rename

	psp_coord_annot_DF = pd.merge(psp_coord, known_coord_cols_to_add, how="left", left_on='VAR', right_on='H_var_coord')
	psp_coord_annot_DF.sort_values(by=['Gene', 'order', 'POS'], ascending=[True, True, True], inplace=True)
	psp_coord_annot_DF.drop(['order'], axis=1, inplace=True)

	known_coord_annot_DF = pd.merge(known_coord, psp_coord_cols_to_add, how="left", left_on='var_coord', right_on='PSP_var_coord')
	known_coord_annot_DF.sort_values(by=['Gene', 'order', 'Start'], ascending=[True, True, True], inplace=True)
	known_coord_annot_DF.drop(['order'], axis=1, inplace=True)

	psp_coord_subtable = psp_coord_annot_DF[select_cols2]
	psp_coord_subtable.columns = select_cols2_rename

	table_file = os.path.join(out_dir, data_name + "-position_Reported" + file_ext)
	psp_file = os.path.join(out_dir, data_name + "-position_Reported_fullPSP" + file_ext)
	hgmdCV_file = os.path.join(out_dir, data_name + "-position_Reported_HGMD-CV" + file_ext)

	psp_coord_subtable.to_excel(table_file, sheet_name='ALL', index=False, header=True)
	psp_coord_annot_DF.to_excel(psp_file, sheet_name='ALL', index=False, header=True)
	known_coord_annot_DF.to_excel(hgmdCV_file, sheet_name='ALL', index=False, header=True)



def outputAllAtPSPposition(psp_coord, known_coord, data_name, file_ext, out_dir, rare_type):
	if "exclusive" in rare_type:
		select_cols2 = ['Gene', 'var_ID', 'H_var_ID', 'MAC',
						'MAF', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
						'C_Significance', 'C_disease', 'H_sift_score', 'H_sift_prediction',
						'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE', 'rsID', 'dbSNP',
						'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported', 'H_accession', 'C_accession',
						'VAR', 'REF', 'ALT', 'H_REF', 'H_ALT', 'H_HGVS_protein', 'C_HGVS_protein' ]
		select_cols2_rename = ['Gene', 'PSP_var_ID', 'H_var_ID',  'MAC',
							   'MAF', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
							   'C_Significance', 'C_disease', 'H_sift_score',
							   'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
							   'PSP_rsID', 'H_rsID','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
							   'H_accession','C_accession', 'PSP_var_coord', 'PSP REF', 'PSP ALT',
							   'H_REF', 'H_ALT', 'H_HGVS_protein', 'C_HGVS_protein']
	
		psp_cols_to_add2 = ['VAR', 'var_ID', 'REF', 'ALT', 'MAC', 'MAF',
							'EXAC MAF', 'rsID', 'SNPEFF_AMINO_ACID_CHANGE']
		psp_cols_rename = ['PSP_var_coord', 'PSP_var_ID', 'PSP_REF', 'PSP_ALT', 'MAC', 'MAF',
							'EXAC MAF', 'PSP_rsID', 'Protein change']
	
	else:
		select_cols2 = ['Gene', 'var_ID', 'H_var_ID', 'MAC',
		                'MAF', 'MAF_ADSP', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
		                'C_Significance', 'C_disease',  'H_sift_score', 'H_sift_prediction',
		                'H_mutpred_score', 'H_Type', 'SNPEFF_AMINO_ACID_CHANGE', 'rsID', 'dbSNP',
		                'H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported', 'H_accession', 'C_accession',
		                'VAR', 'REF', 'ALT', 'H_REF', 'H_ALT', 'H_HGVS_protein', 'C_HGVS_protein' ]
		select_cols2_rename = ['Gene', 'PSP_var_ID', 'H_var_ID',  'MAC',
		                       'MAF', 'ADSP MAF', 'EXAC MAF', 'H_Variant_class', 'H_Disease',
		                       'C_Significance', 'C_disease',  'H_sift_score',
		                       'H_sift_prediction', 'H_mutpred_score', 'H_Type', 'Protein change',
		                       'PSP_rsID', 'H_rsID','H_PMID', 'H_Citation', 'C_OMIM', 'DB_reported',
		                       'H_accession','C_accession', 'PSP_var_coord', 'PSP REF', 'PSP ALT',
		                       'H_REF', 'H_ALT', 'H_HGVS_protein', 'C_HGVS_protein']

		psp_cols_to_add2 = ['VAR', 'var_ID', 'REF', 'ALT', 'MAC', 'MAF', 'MAF_ADSP',
		                    'EXAC MAF', 'rsID', 'SNPEFF_AMINO_ACID_CHANGE']
		psp_cols_rename = ['PSP_var_coord', 'PSP_var_ID', 'PSP_REF', 'PSP_ALT', 'MAC', 'MAF',
		                   'ADSP MAF', 'EXAC MAF', 'PSP_rsID', 'Protein change']


	known_cols_to_add2 = ['Gene', 'var_coord', 'var_ID', 'H_REF', 'H_ALT', 'H_Variant_class',
						  'H_Disease', 'C_Significance', 'C_disease', 'H_Type', 'H_sift_score',
						  'H_sift_prediction', 'H_mutpred_score', 'H_PMID', 'H_Citation', 'C_OMIM',
						  'C_accession', 'H_accession', 'DB_reported', 'dbSNP', 'dbSNP_Validated',
						  'order', 'H_HGVS_protein', 'C_HGVS_protein']
	known_cols_rename = ['Gene', 'H_var_coord', 'H_var_ID', 'H_REF', 'H_ALT', 'H_Variant_class',
						 'H_Disease', 'C_Significance', 'C_disease', 'H_Type', 'H_sift_score',
						 'H_sift_prediction', 'H_mutpred_score', 'H_PMID', 'H_Citation', 'C_OMIM',
						 'C_accession', 'H_accession', 'DB_reported', 'dbSNP', 'dbSNP_Validated',
						 'order', 'H_HGVS_protein', 'C_HGVS_protein']

	psp_coord_cols_to_add = psp_coord[psp_cols_to_add2]
	psp_coord_cols_to_add.columns = psp_cols_rename
	known_coord_cols_to_add = known_coord[known_cols_to_add2]
	known_coord_cols_to_add.columns = known_cols_rename

	psp_coord_annot_DF = pd.merge(psp_coord, known_coord_cols_to_add, how="inner", left_on='VAR', right_on='H_var_coord')
	psp_coord_annot_DF.sort_values(by=['Gene', 'order', 'POS'], ascending=[True, True, True], inplace=True)
	psp_coord_annot_DF.drop(['order'], axis=1, inplace=True)

	known_coord_annot_DF = pd.merge(known_coord, psp_coord_cols_to_add, how="inner", left_on='var_coord', right_on='PSP_var_coord')
	known_coord_annot_DF.sort_values(by=['Gene', 'order', 'Start'], ascending=[True, True, True], inplace=True)
	known_coord_annot_DF.drop(['order'], axis=1, inplace=True)

	psp_coord_subtable = psp_coord_annot_DF[select_cols2]
	psp_coord_subtable.columns = select_cols2_rename

	table_file = os.path.join(out_dir, data_name + "-position_Reported-ALL-vars" + file_ext)
	psp_file = os.path.join(out_dir, data_name + "-position_Reported-ALL-vars_fullPSP" + file_ext)
	hgmdCV_file = os.path.join(out_dir, data_name + "-position_Reported-ALL-vars_HGMD-CV" + file_ext)

	psp_coord_subtable.to_excel(table_file, sheet_name='ALL', index=False, header=True)
	psp_coord_annot_DF.to_excel(psp_file, sheet_name='ALL', index=False, header=True)
	known_coord_annot_DF.to_excel(hgmdCV_file, sheet_name='ALL', index=False, header=True)





def runProgram(pspVarFile, clinicalVarFile, data_name, out_dir, rare_type, reported_only=False):
	
	############# setup ###############
	## dictionary for sorting variants by clinical severity
	varClass_order = {"DM":0, "DM?":1, "DFP":2, "DP":3, "FP":4, "C_only":5}
	
	## setup output file name variables
	curr_date = time.strftime("%m%d%y")
	print(curr_date)
	
	file_ext = "_" + curr_date + ".xlsx"
	print(file_ext)
	
	## load input files to DF
	psp_all_DF = pd.read_excel(pspVarFile)
	known_all_DF = pd.read_excel(clinicalVarFile)
	known_all_DF['order'] = known_all_DF['H_Variant_class'].replace(varClass_order)
	
	
	############# analyze variants ###############
	psp_exactVar_DF, clinical_exactVar_DF, psp_positionReported_DF, \
		clinical_positionReported_DF_DF = compareVariants(psp_all_DF, known_all_DF)
	
	############# 3 possible outputs --> write output files ###############
	if reported_only:
		outputReportedVariants(psp_exactVar_DF, clinical_exactVar_DF, data_name, file_ext, out_dir, rare_type)
	else:
		outputReportedVariants(psp_exactVar_DF, clinical_exactVar_DF, data_name, file_ext, out_dir, rare_type)
		outputKnownPositionVariants(psp_positionReported_DF, clinical_positionReported_DF_DF, data_name, file_ext, out_dir, rare_type)
		outputAllAtPSPposition(psp_all_DF, known_all_DF, data_name, file_ext, out_dir, rare_type)
	


#####################################################################
## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--pspFile",
						dest="psp_var_file", type=valid_file, action='store',
						help="path for the PSP variant file")
	parser.add_argument("-c", "--clinical",
						dest="clinical_var_file", type=valid_file, action='store',
						help="path for the combined ClinVar+HGMD variant file")
	parser.add_argument("-o", "--outDir",
						dest="out_directory", type=str, action='store',
						help="use option '-d' to enter directory for output file")
	parser.add_argument("-n", "--name",
						dest="data_name", type=str, action='store',
						help="Name of data set")
	parser.add_argument('-r', '--reported',
						dest="reported_only", action="store_true", default=False,
						help="output only reported variants?")
	parser.add_argument("--rare_type",
						dest="rare_type", type=str, action='store',
						help="exclusive | extremely rare")
	
	pargs = parser.parse_args()
	psp_var_file = pargs.psp_var_file
	clinical_var_file = pargs.clinical_var_file
	data_name = pargs.data_name
	out_dir = pargs.out_directory
	reported_only = pargs.reported_only
	rare_type = pargs.rare_type
	
	
	runProgram(psp_var_file, clinical_var_file, data_name, out_dir, rare_type, reported_only)
	print("Finished")

