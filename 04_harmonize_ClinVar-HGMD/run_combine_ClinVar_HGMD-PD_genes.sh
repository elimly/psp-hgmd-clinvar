#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/04_harmonize_ClinVar-HGMD

ClinVar_file=$BASE_PATH/ClinVar_PD_genes-LRRK2_020317.xlsx
HGMD_file=$BASE_PATH/HGMD_PD_genes-LRRK2_020317_varID.xlsx
OUTNAME="PD_genes-LRRK2_020317"


python $SRC_PATH/combine_ClinVar_HGMD.py -hg $HGMD_file -c $ClinVar_file -n $OUTNAME -s -d $BASE_PATH

