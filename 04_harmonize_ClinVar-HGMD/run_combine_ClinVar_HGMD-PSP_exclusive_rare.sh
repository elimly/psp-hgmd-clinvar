#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PSP_rare_exclusive
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/04_harmonize_ClinVar-HGMD

ClinVar_file=$BASE_PATH/ClinVar_PSP_rare-exclusive_021317.xlsx
HGMD_file=$BASE_PATH/HGMD_PSP_rare-exclusive_021317_varID.xlsx
OUTNAME="PSP_rare-exclusive_021617"


python $SRC_PATH/combine_ClinVar_HGMD.py -hg $HGMD_file -c $ClinVar_file -n $OUTNAME -s -d $BASE_PATH

