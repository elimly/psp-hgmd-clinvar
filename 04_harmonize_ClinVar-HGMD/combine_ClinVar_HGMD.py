#! /usr/bin/env python
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## myvariant API setup
import myvariant
mv = myvariant.MyVariantInfo()

def compareGenes(cv_all, hgmd):
	summary_list = []

	## extract gene lists:
	clinvar_genes = cv_all['Gene'].drop_duplicates().tolist()
	hgmd_genes = hgmd['Gene'].drop_duplicates().tolist()

	## compare gene lists:
	both_genes = list(set(clinvar_genes).intersection(set(hgmd_genes)))
	clinvar_only_genes = list(set(clinvar_genes).difference(set(hgmd_genes)))
	hgmd_only_genes = list(set(hgmd_genes).difference(set(clinvar_genes)))

	## add gene info to summary list:
	summary_list.append(("GENE COMPARISON:", " "))
	summary_list.append((" ", "# of genes"))
	summary_list.append(("HGMD all", len(hgmd_genes)))
	summary_list.append(("ClinVar all", len(clinvar_genes)))
	summary_list.append(("HGMD & ClinVar", len(both_genes)))
	summary_list.append(("HGMD only", len(hgmd_only_genes)))
	summary_list.append(("ClinVar only", len(clinvar_only_genes)))
	summary_list.append((" ", " "))
	summary_list.append((" ", " "))

	## convert list to dataframe:
	summary_DF = pd.DataFrame(summary_list)
	return summary_DF



def reportedDB(inBoth, c_only, h_only, cv_all, hgmd):
	b_df = hgmd.loc[hgmd['var_ID'].isin(inBoth), ['Gene', 'var_ID']]
	h_df = hgmd.loc[hgmd['var_ID'].isin(h_only), ['Gene', 'var_ID']]
	c_df = cv_all.loc[cv_all['var_ID'].isin(c_only), ['Gene', 'var_ID']]
	b_df['DB_reported'] = "both"
	h_df['DB_reported'] = "HGMD"
	c_df['DB_reported'] = "ClinVar"
	db_df = pd.concat([b_df, h_df, c_df])
	db_df = db_df.drop_duplicates()
	return db_df



def compareVariants(cv_all, cv_patho, hgmd):
	summary_list = []

	## extract variant lists:
	clinvar_vars_all = cv_all['var_ID'].drop_duplicates().tolist()
	clinvar_vars_patho = cv_patho['var_ID'].drop_duplicates().tolist()
	hgmd_vars = hgmd['var_ID'].drop_duplicates().tolist()

	############# compare variant lists ###############
	## vars in BOTH HGMD & ClinVar:
	both_vars = list(set(clinvar_vars_all).intersection(set(hgmd_vars)))
	## vars only in ClinVar:
	clinvar_only_vars_all = list(set(clinvar_vars_all).difference(set(hgmd_vars)))
	clinvar_only_vars_patho = list(set(clinvar_vars_patho).difference(set(hgmd_vars)))
	## vars only in HGMD:
	hgmd_only_vars = list(set(hgmd_vars).difference(set(clinvar_vars_all)))

	## add gene & variant info to summary list:
	summary_list.append((" ", " "))
	summary_list.append((" ", " "))
	summary_list.append(("VARIANT COMPARISON:", ""))
	summary_list.append((" ", "# of variants "))
	summary_list.append(("HGMD all", len(hgmd_vars)))
	summary_list.append(("ClinVar all", len(clinvar_vars_all)))
	summary_list.append(("ClinVar pathogenic", len(clinvar_vars_patho)))
	summary_list.append(("HGMD & ClinVar", len(both_vars)))
	summary_list.append(("HGMD only", len(hgmd_only_vars)))
	summary_list.append(("ClinVar only (all)", len(clinvar_only_vars_all)))
	summary_list.append(("ClinVar only (pathogenic)", len(clinvar_only_vars_patho)))

	## convert summary list to dataframe:
	summary_DF = pd.DataFrame(summary_list)

	## store [variant, database source] in dataframe:
	db_df = reportedDB(both_vars, clinvar_only_vars_patho, hgmd_only_vars, cv_all, hgmd)
	return (db_df, summary_DF)



def processClinVarDF(cv_all, hgmd):
	patho_labels = ['Pathogenic, association', 'Pathogenic, risk factor', 'Likely pathogenic',
					'Pathogenic', 'risk factor', 'drug response']

	## extract pathogenic variants from ClinVar dataframe:
	cv_patho_DF = cv_all[cv_all['ClinVar_Significance'].isin(patho_labels)]

	## extract non-pathogenic variants from ClinVar dataframe:
	cv_nonPatho_DF = cv_all[~cv_all['ClinVar_Significance'].isin(patho_labels)]

	## remove ClinVar non-pathogenic variants NOT found in HGMD:
	cv_nonPatho_DF = cv_nonPatho_DF[cv_nonPatho_DF['var_ID'].isin(hgmd['var_ID'])]

	## keep ALL pathogenic ClinVar + only non-pathogenic ClinVar that are in HGMD
	cv_DF = pd.concat([cv_patho_DF, cv_nonPatho_DF])

	cv_DF['C_REF'] = cv_DF['REF']
	cv_DF['C_ALT'] = cv_DF['ALT']
	cv_DF.drop(['REF', 'ALT'], axis=1, inplace=True)

	## compare ClinVar & HGMD variants:
	var_compare = compareVariants(cv_all, cv_patho_DF, hgmd)
	summary_DF = var_compare[1]
	db_df = var_compare[0]
	return (cv_DF, db_df, summary_DF)



def dbSNPquery(varList):
	query_list = []
	for v in varList:
		## retrieve variant info
		var = mv.getvariant(v, fields=['dbsnp.rsid', 'dbsnp.validated'])
		rsid = "none"
		valid = "n/a"

		## extract variant annotation
		if (type(var) is dict):
			if ('dbsnp' in var):
				rsid = var['dbsnp']['rsid']
				valid = var['dbsnp']['validated']
		query_list.append((v, rsid, valid))

	## convert list to Dataframe:
	query_DF = pd.DataFrame(query_list, columns=['var_ID', 'dbSNP', 'dbSNP_Validated'])
	query_DF = query_DF.drop_duplicates()
	return query_DF



def combineClinvarHGMD(clinvar_all_DF, hgmd_DF, batch, col_name_dict):
	## set up column variables
	merge_col = col_name_dict['merge_cols']
	if 'hgvs_protein' in clinvar_all_DF.columns:
		CV_only_cols = col_name_dict['cols_CV_only']
	else:
		CV_only_cols = col_name_dict['cols_CV_only2']
	print(CV_only_cols)
	
	if batch:
		hgmd_type = 'batch'
	else:
		hgmd_type = 'gene'
	cols_H_only = col_name_dict[hgmd_type]['cols_H_only']
	cols_order = col_name_dict[hgmd_type]['cols_reorder']
	cols_name = col_name_dict[hgmd_type]['cols_rename']

	summary_DF = compareGenes(clinvar_all_DF, hgmd_DF)

	## prepare the ClinVar dataframe:
	processed = processClinVarDF(clinvar_all_DF, hgmd_DF)
	clinvar_DF = processed[0]
	db_df = processed[1]
	## join with summary DF
	summary_DF = pd.concat([summary_DF, processed[2]])
	# print(summary_DF.head())

	## merge HGMD & ClinVar dataframes:
	clinvar_DF.drop(['Gene'], axis=1, inplace=True)
	hgmd_DF.drop(['Gene'], axis=1, inplace=True)

	merge_DF = pd.merge(hgmd_DF, clinvar_DF, how="outer", on=merge_col)
	merge_DF = merge_DF.drop_duplicates()

	## format columns in merged DF (fill in NA cells & delete unnecessary columns):
	merge_DF[CV_only_cols] = merge_DF[CV_only_cols].fillna('H_only')
	merge_DF[cols_H_only] = merge_DF[cols_H_only].fillna('C_only')
	merge_DF.drop(['rsID', 'dbSNP_Validated'], axis=1, inplace=True)
	if batch == False:
		merge_DF.drop(['dbsnp'], axis=1, inplace=True)

	## extract list of variant IDs:
	vars_combined = merge_DF['var_ID']

	## retrieve dbSNP annotation for list of variants:
	dbsnp_DF = dbSNPquery(vars_combined)

	## add dbSNP annotation to merged DF:
	merge_DF = pd.merge(merge_DF, dbsnp_DF, how="left", on='var_ID')
	merge_DF = merge_DF.drop_duplicates()

	## add Reported database annotation to merged DF:
	merge_DF = pd.merge(merge_DF, db_df, how="left", on='var_ID')
	merge_DF = merge_DF.drop_duplicates()

	## reorder and rename the dataframe columns:
	merge_DF = merge_DF[cols_order]
	merge_DF.columns = cols_name
	merge_DF.sort_values(by=['var_ID'], ascending=[True], inplace=True)

	return (merge_DF, summary_DF)




def runProgram(clinVar_file, HGMD_file, outName, outDir, bool_batch, bool_single, col_name_dict):
	## create output file names:
	var_outFile = os.path.join(outDir, outName + "_HGMD_ClinVar_variants.xlsx")
	summary_outFile = os.path.join(outDir, outName + "_HGMD_ClinVar_summary.xlsx")
	
	## read in ClinVar and HGMD variant files --> pandas dataframes:
	clinvar_all_in_DF = pd.read_excel(clinVar_file)
	hgmd_in_DF = pd.read_excel(HGMD_file)

	## combine ClinVar and HGMD variants:
	combined = combineClinvarHGMD(clinvar_all_in_DF, hgmd_in_DF, bool_batch, col_name_dict)
	var_DF = combined[0]
	summary_DF = combined[1]

	## write output files:
	var_DF.to_excel(var_outFile, sheet_name='Orig', index=False, header=True)
	summary_DF.to_excel(summary_outFile, sheet_name='summary', index=False, header=False)
	

#####################################################################
merge_cols = ['var_ID', 'Chr', 'Start', 'var_coord', 'variant']

cols_CV_only = ['rsID', 'ClinVar_Significance', 'ClinVar_disease',
				'ClinVar_synonyms', 'OMIM_ID', 'ClinVar_accession', 'ClinVar_date', 'ClinVar_name',
				'dbSNP_Validated', 'C_REF', 'C_ALT', 'hgvs_coding', 'hgvs_protein']

cols_CV_only2 = ['rsID', 'ClinVar_Significance', 'ClinVar_disease',
				'ClinVar_synonyms', 'OMIM_ID', 'ClinVar_accession', 'ClinVar_date', 'ClinVar_name',
				'dbSNP_Validated', 'C_REF', 'C_ALT']

## HGMD individual gene query
cols_H_only_gene = ['Variant_class', 'ACC_NUM', 'dbsnp', 'disease',
			   'genomic_coordinates_hg19', 'sift_score', 'sift_prediction', 'mutpred_score', 'pmid', 'Citation',
			   'HGVS_cdna', 'HGVS_protein', 'entrezid', 'sequence_context_hg19', 'Type', 'codon_change',
			   'codon_number', 'intron_number', 'site', 'REF', 'ALT']

cols_reorder_gene = ['Gene', 'var_ID', 'DB_reported', 'variant',  'Chr', 'Start',
				'var_coord', 'dbSNP', 'dbSNP_Validated', 'Variant_class', 'disease', 'Type',
				'sift_score', 'sift_prediction', 'mutpred_score', 'pmid', 'Citation',
				'genomic_coordinates_hg19', 'sequence_context_hg19', 'HGVS_cdna', 'HGVS_protein',
				'codon_change', 'codon_number', 'intron_number', 'site', 'ACC_NUM', 'entrezid',
				'ClinVar_Significance', 'ClinVar_disease', 'ClinVar_synonyms', 'OMIM_ID',
				'ClinVar_accession', 'ClinVar_date', 'ClinVar_name', 'hgvs_coding', 'hgvs_protein', 'C_REF',
					 'C_ALT', 'REF', 'ALT', 'Strand']

cols_rename_gene = ['Gene', 'var_ID', 'DB_reported', 'variant',   'Chr', 'Start',
			   'var_coord', 'dbSNP', 'dbSNP_Validated', 'H_Variant_class', 'H_Disease', 'H_Type',
			   'H_sift_score', 'H_sift_prediction', 'H_mutpred_score', 'H_PMID', 'H_Citation', 'H_hg19_coord',
			   'H_hg19_sequence', 'H_HGVS_cdna', 'H_HGVS_protein', 'H_codon_change', 'H_codon_#',
			   'H_intron_#', 'H_site', 'H_accession', 'H_EntrezId', 'C_Significance', 'C_disease',
			   'C_synonyms', 'C_OMIM', 'C_accession', 'C_date', 'C_name', 'C_HGVS_cDNA', 'C_HGVS_protein',
				'C_REF', 'C_ALT', 'H_REF', 'H_ALT', 'H_Strand' ]


## HGMD batch mutation mart search:
cols_H_only_batch = ['HGMD ID', 'Disease', 'Variant Class', 'strand', 'hgvs', 'End']

cols_reorder_batch = ['Gene', 'var_ID', 'DB_reported', 'variant', 'REF', 'ALT', 'Chr', 'Start',
					  'var_coord', 'dbSNP', 'dbSNP_Validated', 'Variant Class', 'Disease', 'hgvs',
					  'End', 'strand', 'HGMD ID', 'ClinVar_Significance', 'ClinVar_disease',
					  'ClinVar_synonyms', 'OMIM_ID', 'ClinVar_accession', 'ClinVar_date',
					  'ClinVar_name']

cols_rename_batch = ['Gene', 'var_ID', 'DB_reported', 'variant', 'REF', 'ALT', 'Chr', 'Start',
					 'var_coord', 'dbSNP', 'dbSNP_Validated', 'H_Variant_class', 'H_Disease',
					 'H_hgvs', 'H_End', 'H_strand', 'H_accession', 'C_Significance',
					 'C_disease', 'C_synonyms', 'C_OMIM', 'C_accession', 'C_date', 'C_name']


col_dict = {'merge_cols': merge_cols, 'cols_CV_only': cols_CV_only, 'cols_CV_only2': cols_CV_only2}
col_dict['gene'] = {'cols_H_only': cols_H_only_gene, 'cols_reorder': cols_reorder_gene, 'cols_rename':cols_rename_gene}
col_dict['batch'] = {'cols_H_only': cols_H_only_batch, 'cols_reorder': cols_reorder_batch, 'cols_rename':cols_rename_batch}

#####################################################################
## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-hg", "--hgmd",
						dest="hgmdFile", type=valid_file, action='store',
						help="path for the HGMD variant file")
	parser.add_argument("-c", "--clinvar",
						dest="clinvarFile", type=valid_file, action='store',
						help="path for the ClinVar variant file")
	parser.add_argument("-d", "--directory",
						dest="directory", type=str, action='store',
						help="use option '-d' to enter directory for output file")
	parser.add_argument("-n", "--name",
						dest="outName", type=str, action='store',
						help="Name of Gene or Batch Query")
	parser.add_argument('-b', action="store_true", default=False,
						help="Batch search file?")
	parser.add_argument('-s', action="store_true", default=False,
						help="Single gene file?")
	
	pargs = parser.parse_args()
	
	clinVar_file = pargs.clinvarFile
	HGMD_file = pargs.hgmdFile
	outName = pargs.outName
	outDir = pargs.directory
	
	bool_batch = pargs.b
	bool_single = pargs.s
	
	runProgram(clinVar_file, HGMD_file, outName, outDir, bool_batch, bool_single, col_dict)
	
	print("Finished")


