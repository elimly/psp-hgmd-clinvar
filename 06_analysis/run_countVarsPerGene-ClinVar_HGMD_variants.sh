#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/06_analysis

VAR_FILE=$BASE_PATH/PD_genes-LRRK2_020317_HGMD_ClinVar_variants.xlsx
OUT_DIR=$BASE_PATH

OUTFILE_NAME="PD_genes_var_counts.xlsx"


python $SRC_PATH/countReportedVarsPerGene.py -v $VAR_FILE -o $OUTFILE_NAME -d $OUT_DIR 
