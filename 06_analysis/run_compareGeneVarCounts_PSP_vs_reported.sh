#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/06_analysis

PSP_ALL_VAR_FILE=$BASE_PATH/PSP_variants_PD_genes-LRRK2_012717.xlsx

PSP_REPORTED_VAR_FILE=$BASE_PATH/clinical/exact_variant_reported/PSP_PD_genes_Reported_012717.xlsx

CLINVAR_HGMD_VAR_FILE=$BASE_PATH/PD_genes-LRRK2_020317_HGMD_ClinVar_variants.xlsx

OUT_DIR=$BASE_PATH
OUTFILE_PREFIX="PSP_PD_genes_var_count_comparison_"


python $SRC_PATH/compareGeneVarCounts_PSP_vs_reported.py --psp_all $PSP_ALL_VAR_FILE --psp_reported $PSP_REPORTED_VAR_FILE --reported $CLINVAR_HGMD_VAR_FILE -o $OUTFILE_PREFIX -d $OUT_DIR 
