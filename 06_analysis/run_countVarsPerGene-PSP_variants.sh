#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP/PD_genes
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/06_analysis

VAR_FILE=$BASE_PATH/clinical/exact_variant_reported/PSP_PD_genes_Reported_012717.xlsx
OUT_DIR=$BASE_PATH/clinical

OUTFILE_NAME="PSP_PD_genes_var_counts.xlsx"


python $SRC_PATH/countReportedVarsPerGene.py -v $VAR_FILE -o $OUTFILE_NAME -d $OUT_DIR 
