#! /usr/bin/env python

from functools import partial
import time, os, argparse

import pandas as pd
pd.options.mode.chained_assignment = None



#################################################
#### Helper fxns
def countVarsDB(db, varDF, row):
	gene = str(row['Gene', 'Gene'])
	return varDF.loc[(varDF['DB_reported'] == db) & (varDF['Gene'] == gene), ['Gene', 'var_ID']].shape[0]

def countPSPvars(varDF, row):
	gene = str(row['Gene', 'Gene'])
	return varDF.loc[(varDF['SNPEFF_GENE_NAME'] == gene), ['var_ID']].shape[0]

def countVarsGene(varDF, row):
	gene = str(row['Gene', 'Gene'])
	return varDF.loc[(varDF['Gene'] == gene), ['Gene', 'var_ID']].shape[0]

#################################################




def initializeCountDF(clinvar_hgmd_df):
	## extract gene list
	clinvar_hgmd_gene_list = clinvar_hgmd_df['Gene'].sort_values().drop_duplicates()
	
	## column name tuple list - for multiindex
	c_idx = list(zip([ 'Gene', 'Reported', 'Reported', 'Reported', 'Reported', 'PSP', 'PSP', 'PSP', 'PSP', 'PSP'],
				 [ 'Gene', 'Total', 'both', 'H only', 'CV only', 'Total_Detected', 'Reported', 'both', 'H only', 'CV only'] ))
	
	## generate Pandas multiindex object from tuple list
	count_col_multiindex = pd.MultiIndex.from_tuples(c_idx)
	
	## generate new multiindex DF from gene list
	multiindex_count_df = pd.DataFrame(index=clinvar_hgmd_gene_list.tolist(), columns=count_col_multiindex)
	multiindex_count_df['Gene', 'Gene'] = multiindex_count_df.index
	
	return multiindex_count_df


def compareVarCounts(gene_count_df, psp_all_df, psp_reported_df, clinvar_hgmd_df):
	gene_count_df['Reported','Total'] = gene_count_df.apply(partial(countVarsGene, clinvar_hgmd_df), axis=1)
	gene_count_df['Reported','CV only'] = gene_count_df.apply(partial(countVarsDB, 'ClinVar', clinvar_hgmd_df), axis=1)
	gene_count_df['Reported','H only'] = gene_count_df.apply(partial(countVarsDB, 'HGMD', clinvar_hgmd_df), axis=1)
	gene_count_df['Reported','both'] = gene_count_df.apply(partial(countVarsDB, 'both', clinvar_hgmd_df), axis=1)
	
	gene_count_df['PSP','Total_Detected'] = gene_count_df.apply(partial(countPSPvars, psp_all_df), axis=1)
	gene_count_df['PSP','Reported'] = gene_count_df.apply(partial(countVarsGene, psp_reported_df), axis=1)
	gene_count_df['PSP','CV only'] = gene_count_df.apply(partial(countVarsDB, 'ClinVar', psp_reported_df), axis=1)
	gene_count_df['PSP','H only'] = gene_count_df.apply(partial(countVarsDB, 'HGMD', psp_reported_df), axis=1)
	gene_count_df['PSP','both'] = gene_count_df.apply(partial(countVarsDB, 'both', psp_reported_df), axis=1)
	
	return gene_count_df


def runComparison(psp_all_file, psp_reported_file, clinvar_hgmd_file, out_dir, out_name):
	## load input files
	psp_all_df = pd.read_excel(psp_all_file)
	psp_reported_df = pd.read_excel(psp_reported_file)
	clinvar_hgmd_df = pd.read_excel(clinvar_hgmd_file)
	
	## initialize gene var count multiindex DF
	gene_count_df = initializeCountDF(clinvar_hgmd_df)
	
	## compare PSP vs Reported var counts
	gene_count_df = compareVarCounts(gene_count_df, psp_all_df, psp_reported_df, clinvar_hgmd_df)
	
	## generate output file name with today's date
	curr_date = time.strftime("%m%d%y")
	file_name = out_name + curr_date + ".xlsx"
	output_file = os.path.join(out_dir, file_name)

	## write output file
	gene_count_df.to_excel(output_file, sheet_name='var counts', header=True)
	
	




## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-pa", "--psp_all",
						dest="psp_ALL_var_file", type=valid_file, action='store',
						help="path for the PSP PD gene variant file (ALL variants)")
	parser.add_argument("-pr", "--psp_reported",
						dest="psp_reported_var_file", type=valid_file, action='store',
						help="path for the PSP reported PD gene variants file")
	
	parser.add_argument("-r", "--reported",
						dest="clinvar_hgmd_var_file", type=valid_file, action='store',
						help="path for the combined ClinVar+HGMD variant file")
	
	parser.add_argument("-d", "--dir_out",
						dest="out_dir", type=str, action='store',
						help="directory path for the output file")
	parser.add_argument("-o", "--out_name",
						dest="out_name", type=str, action='store',
						help="prefix to use in name of the resulting output file")
	
	pargs = parser.parse_args()
	psp_ALL_var_file = pargs.psp_ALL_var_file
	psp_reported_var_file = pargs.psp_reported_var_file
	clinvar_hgmd_var_file = pargs.clinvar_hgmd_var_file
	output_dir = pargs.out_dir
	output_name = pargs.out_name
	
	runComparison(psp_ALL_var_file, psp_reported_var_file, clinvar_hgmd_var_file, output_dir, output_name)
	print("Finished")

