#! /usr/bin/env python
from functools import partial
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None



#################################################
#### Helper fxns
def countVarsDB1(db, varDF, row):
	gene = str(row['Gene'])
	return varDF.loc[(varDF['DB_reported'] == db) & (varDF['Gene'] == gene), ['Gene', 'var_ID']].shape[0]

def countVarsDB2(db1, db2, varDF, row):
	gene = str(row['Gene'])
	return varDF.loc[((varDF['DB_reported'] == db1) | (varDF['DB_reported'] == db2)) & (varDF['Gene'] == gene), ['Gene', 'var_ID']].shape[0]

def countVarsGene(varDF, row):
	gene = str(row['Gene'])
	return varDF.loc[(varDF['Gene'] == gene), ['Gene', 'var_ID']].shape[0]

#################################################



def initializeDFs(var_input_file):
	## load input file
	var_df = pd.read_excel(var_input_file)
	var_df = var_df.iloc[var_df.Gene.str.lower().argsort()]
	
	## extract gene list
	input_gene_list = var_df['Gene'].drop_duplicates()
	
	## generate new DF from gene list
	cols_gene_var_count_df = ['Gene', 'Total', 'both', 'CV only', 'H only', 'CV all', 'H all']
	gene_var_count_df = pd.DataFrame(data=input_gene_list, columns=cols_gene_var_count_df)
	
	return var_df, gene_var_count_df



def getVarCountsPerGene(var_df, gene_count_df):
	## get overall Total counts (ClinVar + HGMD)
	gene_count_df['Total'] = gene_count_df.apply(partial(countVarsGene, var_df), axis=1)
	
	## count each subset separately
	gene_count_df['CV only'] = gene_count_df.apply(partial(countVarsDB1, 'ClinVar', var_df), axis=1)
	gene_count_df['H only'] = gene_count_df.apply(partial(countVarsDB1, 'HGMD', var_df), axis=1)
	gene_count_df['both'] = gene_count_df.apply(partial(countVarsDB1, 'both', var_df), axis=1)
	
	## get ClinVar & HGMD totals
	gene_count_df['CV all'] = gene_count_df.apply(partial(countVarsDB2, 'ClinVar', 'both', var_df), axis=1)
	gene_count_df['H all'] = gene_count_df.apply(partial(countVarsDB2, 'HGMD', 'both',  var_df), axis=1)

	# print(gene_count_df)
	return gene_count_df



def runProgram(var_input_file, outfile_name, out_dir):
	## load input file & initialize count DF
	var_df, gene_var_count_df = initializeDFs(var_input_file)
	
	## count # of vars per gene
	counted_df = getVarCountsPerGene(var_df, gene_var_count_df)
	print(counted_df)
	
	## write output file
	counted_df.to_excel(os.path.join(out_dir, outfile_name),
	                    sheet_name='var counts', index=False, header=True)
	
	
	
#####################################################################

## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--var_file", type=valid_file, action='store',
	                    help="path for HGMD file")
	parser.add_argument("-o", "--outfile_name",
						dest="outfile_name", type=str, action='store',
						help="name for the resulting gene variant count output file - MUST include file extension!")
	parser.add_argument("-d", "--dir_out",
						dest="out_dir", type=str, action='store',
						help="directory path for the output file")
	
	pargs = parser.parse_args()
	var_file = pargs.var_file
	output_file_name = pargs.outfile_name
	output_dir = pargs.out_dir

	runProgram(var_file, output_file_name, output_dir)
	
	print("Finished.")
