#! /usr/bin/env python
import argparse, os

import pandas as pd
pd.options.mode.chained_assignment = None

## query API - mygene - setup
import mygene
mg = mygene.MyGeneInfo()



def runProgram(geneListFile, outDir, dataName):
	## load input file to DF
	geneList_DF = pd.read_excel(geneListFile)
	
	## extract list of gene symbols
	geneSymbol = geneList_DF['Gene Symbol'].tolist()
	
	## run mygene query
	mygene_query = mg.querymany(geneSymbol, scope="symbol", fields=["entrezgene"], species="human", as_dataframe=True)
	mygene_query['Query'] = mygene_query.index
	
	## extract results to Pandas DF
	mygene_query_DF = mygene_query[['Query', 'entrezgene']]

	## join Entrez IDs with original input file
	gene_symbol_DF = pd.merge(geneList_DF, mygene_query_DF, how="left", left_on=['Gene Symbol'], right_on=['Query'])
	gene_symbol_DF['entrezgene'] = gene_symbol_DF['entrezgene'].fillna(0).astype(int)
	gene_symbol_DF.columns=[ 'Gene Symbol', 'Disease(s)', 'Comment', 'Inheritance', 'Query', 'Entrez_ID']
	del gene_symbol_DF['Query']
	
	gene_symbol_DF = gene_symbol_DF[['Gene Symbol', 'Entrez_ID', 'Disease(s)', 'Comment', 'Inheritance']]
	# print(gene_symbol_DF.head(15))
	
	## extract list of Entrez IDs
	entrezIDs = gene_symbol_DF['Entrez_ID'].copy()
	
	## write output files
	excel_out_file = os.path.join(outDir, dataName + "_EntrezID.xlsx")
	list_out_file = os.path.join(outDir, dataName + "_EntrezIDs.txt")
	
	gene_symbol_DF.to_excel(excel_out_file, sheet_name='Entrez_ID', index=False, header=True)
	entrezIDs.to_csv(list_out_file, sep='\t', header=False, index=False)




#####################################################################
## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-g", "--geneList",
						dest="gene_list_file", type=valid_file, action='store',
						help="path for the gene list file")
	
	parser.add_argument("-o", "--outDir",
						dest="out_dir", type=str, action='store',
						help="directory for output files")
	
	parser.add_argument("-d", "--dataset",
						dest="dataset_name", type=str, action='store',
						help="Name for the current dataset")
	
	pargs = parser.parse_args()
	
	gene_list_file = pargs.gene_list_file
	out_dir = pargs.out_dir
	dataset_name = pargs.dataset_name
	
	runProgram(gene_list_file, out_dir, dataset_name)
	print("Finished")

