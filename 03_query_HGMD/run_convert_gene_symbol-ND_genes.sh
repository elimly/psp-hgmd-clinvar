#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/03_query_HGMD

GENE_LIST_FILE=$BASE_PATH/ClinVar_HGMD_PSP_analysis/ND_gene_lists/ND_disease_gene_list.xlsx
OUT_DIR=$BASE_PATH/ClinVar_HGMD_PSP_analysis/ND_gene_lists
DATA_NAME="ND_disease_gene_list"

python $SRC_PATH/convert_gene_symbol.py -g $GENE_LIST_FILE -o OUT_DIR -d $DATA_NAME
