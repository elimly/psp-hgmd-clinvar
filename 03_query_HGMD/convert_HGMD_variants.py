#!/usr/bin/env python
from functools import partial
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None


cols_query = ['var_ID', 'Gene', 'Variant_class', 'ACC_NUM', 'dbsnp', 'disease',
		'genomic_coordinates_hg19', 'sift_score', 'sift_prediction', 'mutpred_score', 
		'pmid', 'Citation', 'HGVS_cdna', 'HGVS_protein', 'entrezid',
		'sequence_context_hg19', 'Type', 'codon_change', 'codon_number',
		'intron_number', 'site', 'Chr', 'Start', 'variant', 'var_coord',
		'REF', 'ALT', 'Strand']

nucleotide_convert = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}


#################################################
#### Helper fxns

def extractVariant(row):
	strand = str(row['Strand'])
	ref = str(row['REF'])
	alt = str(row['ALT'])

	if '-' in strand:
		ref = nucleotide_convert[ref]
		alt = nucleotide_convert[alt]
	return ref + '>' + alt

def extractVariantID(row):
	chr = str(row['Chr'])
	position = str(row['Start'])
	ref = str(row['variant'])
	return chr + ":g." + str(position) + ref

def extractVariantPos(row):
	chr = str(row['Chr'])
	position = str(row['Start'])
	return chr + ":" + str(position)

def addCitation(row):
	year = "{:.0f}".format(row['year'])
	journal = str(row['journal'])
	vol = str(row['vol'])
	pg = str(row['page'])
	return row['author'] + " (" + year + ") " + journal + " " + vol + ": " + pg

#################################################



def runMutationMart(inputFile, missingFile):
	remove = ['del', 'ins', 'dup']
	mMart_in_DF = pd.read_excel(inputFile)

	## remove rows where ['chromosome'] = null:
	mMart_in_DF = mMart_in_DF[mMart_in_DF['chromosome'] != "null"]

	## extract rows without variant nucleotide change information --> look up in HGMD:
	no_variant_info_DF = mMart_in_DF[mMart_in_DF['hgvs'] == "null"]

	## remove rows without variant nucleotide change information:
	mMart_in_DF = mMart_in_DF[mMart_in_DF['hgvs'] != "null"]

	## remove insertion, deletion, duplication variants:
	mMart_in_DF = mMart_in_DF[~mMart_in_DF['hgvs'].str.contains('|'.join(remove))]

	## extract & reformat variant nucleotide change:
	mMart_in_DF['variant'] = mMart_in_DF['hgvs'].str.replace('c.','')
	mMart_in_DF['variant'] = mMart_in_DF['variant'].str.replace(r'\d', '')
	mMart_in_DF['variant'] = mMart_in_DF['variant'].str.replace('+', '')
	mMart_in_DF['variant'] = mMart_in_DF['variant'].str.replace('-', '')

	## rename dataframe columns:
	mMart_in_DF['Chr'] = mMart_in_DF['chromosome']
	mMart_in_DF['Start'] = mMart_in_DF['coordinate start']
	mMart_in_DF['End'] = mMart_in_DF['coordinate end']
	mMart_in_DF['Gene'] = mMart_in_DF['Gene Symbol']
	mMart_in_DF.drop(['Gene Symbol', 'chromosome', 'coordinate start', 'coordinate end'], axis=1, inplace=True)


	## convert variant ID format (chr_:g._coordinate__Ref_>_Alt_)
	mMart_in_DF['var_ID'] = mMart_in_DF.apply(partial(extractVariantID), axis=1)

	## convert variant position format (chr_:_coordinate_)
	mMart_in_DF['var_coord'] = mMart_in_DF.apply(partial(extractVariantPos), axis=1)

	## extract REF && ALT nucleotides:
	mMart_in_DF['REF'], mMart_in_DF['ALT'] = mMart_in_DF['variant'].str.split('>').str

	no_variant_info_DF.to_excel(missingFile, sheet_name='Orig', index=False, header=True)
	return mMart_in_DF



def runHGMDquery(inputFile):
	query_in_DF = pd.read_excel(inputFile)

	## remove rows without Entrez ID:
	query_in_DF = query_in_DF[query_in_DF['entrezid'] > 0]

	## remove rows without chr coordinates:
	query_in_DF = query_in_DF[query_in_DF['genomic_coordinates_hg19'] != "null"]

	## rename dataframe columns:
	query_in_DF['Gene'] = query_in_DF['gene']
	query_in_DF.drop('gene', axis=1, inplace=True)

	## convert Variant Type format (# code --> string)
	var_type = {1:'missense/nonsense', 2:'splicing', 3:'regulatory'}
	query_in_DF['Type'] = query_in_DF['Type'].replace(var_type)

	## extract chromosome, start, strand:
	query_in_DF['Chr'], query_in_DF['Start'], query_in_DF['Strand'] = query_in_DF['genomic_coordinates_hg19'].str.split(':').str

	## extract & reformat variant nucleotide change:
	query_in_DF['variant'] = query_in_DF['sequence_context_hg19'].str.split('\[|\]').str[1]
	
	## extract REF && ALT nucleotides:
	query_in_DF['REF'], query_in_DF['ALT'] = query_in_DF['variant'].str.split('/').str
	query_in_DF['variant'] = query_in_DF.apply(partial(extractVariant), axis=1)

	## convert variant ID format (chr_:g._coordinate__Ref_>_Alt_)
	query_in_DF['var_ID'] = query_in_DF.apply(partial(extractVariantID), axis=1)

	## convert variant position format (chr_:_coordinate_)
	query_in_DF['var_coord'] = query_in_DF.apply(partial(extractVariantPos), axis=1)

	## add Citation information
	query_in_DF['Citation'] = query_in_DF.apply(partial(addCitation), axis=1)
	print(query_in_DF.head())

	query_out_DF = query_in_DF[cols_query]

	return query_out_DF




def runProgram(inFile, bool_batch, bool_single, outDirFound, outDirNotFound="missing_var_info"):
	## generate output file names
	fileName = os.path.basename(inFile).replace('.xlsx', '')
	outFile_found = os.path.join(outDirFound, fileName + "_varID.xlsx")

	if bool_batch:
		outFile_notFound = os.path.join(outDirNotFound, fileName + "_noVarInfo.xlsx")

		df = runMutationMart(inFile, outFile_notFound)
		df.to_excel(outFile_found, sheet_name='Orig', index=False, header=True)

	elif bool_single:
		df = runHGMDquery(inFile)
		df.to_excel(outFile_found, sheet_name='Orig', index=False, header=True)

#####################################################################

## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("inFile", type=valid_file, action='store',
	                    help="path for HGMD file")
	
	parser.add_argument('-b', action="store_true", default=False,
						help="Batch search file?")
	parser.add_argument('-s', action="store_true", default=False,
						help="Single gene file?")
	parser.add_argument("-o", "--outFound",
						dest="out_dir_found", type=str, action='store',
						help="directory for output varID file")
	parser.add_argument("-n", "--notFound",
						dest="out_dir_not_found", type=str, action='store',
	                    default=None,
						help="directory for missing variant output file")
	
	pargs = parser.parse_args()
	hgmd_input_file = pargs.inFile
	bool_batch = pargs.b
	bool_single = pargs.s
	out_dir_found = pargs.out_dir_found
	out_dir_not_found = pargs.out_dir_not_found
	
	runProgram(hgmd_input_file, bool_batch, bool_single, out_dir_found, out_dir_not_found)
	
	print("Finished")




