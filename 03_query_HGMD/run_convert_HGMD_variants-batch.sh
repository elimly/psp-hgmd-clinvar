#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/03_query_HGMD

BatchFile=$BASE_PATH/ClinVar_HGMD_PSP_analysis/HGMD/HGMD_query_output/HGMD_MutationMart_PD_genes.xlsx

python $SRC_PATH/convert_HGMD_variants.py $BatchFile -b
