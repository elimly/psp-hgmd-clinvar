#!/bin/bash

echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/03_query_HGMD

HGMD_RESULT_FILE=$BASE_PATH/ClinVar_HGMD_PSP_analysis/HGMD/HGMD_query_output/gene_results/HGMD_PD_genes_011917.xlsx
OUT_DIR_FOUND=$BASE_PATH/ClinVar_HGMD_PSP_analysis/HGMD/varIDs
OUT_DIR_NOT_FOUND=$BASE_PATH/ClinVar_HGMD_PSP_analysis/HGMD/missing_var_info

python $SRC_PATH/convert_HGMD_variants.py $HGMD_RESULT_FILE -s -o $OUT_DIR_FOUND -n $OUT_DIR_NOT_FOUND

