#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/01_convert_PSP_variant_format

PSP_VAR_FILE_EXTREMELY_RARE=$BASE_PATH/data_files_recd/orig_variant_lists_Jacky/PSP_extremely_rare_recurring_1021.xlsx

PSP_VAR_FILE_EXCLUSIVE=$BASE_PATH/data_files_recd/orig_variant_lists_Jacky/PSP_exclusive_recurring_1021.xlsx

OUT_FILE_EXTREMELY_RARE=$BASE_PATH/PSP_rare_exclusive/PSP_extremely_rare_recurring_varIDs.txt

OUT_FILE_EXCLUSIVE=$BASE_PATH/PSP_rare_exclusive/PSP_exclusive_recurring_varIDs.txt



## run with PSP Extremely Rare variants
python $SRC_PATH/psp_convert_variants_for_ClinVar.py --var_rare PSP_VAR_FILE_EXTREMELY_RARE --var_exclusive PSP_VAR_FILE_EXCLUSIVE --out_rare OUT_FILE_EXTREMELY_RARE --out_exclusive OUT_FILE_EXCLUSIVE



