#! /usr/bin/env python
from functools import partial
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## ClinVar query API - myvariant - setup
import myvariant
mv = myvariant.MyVariantInfo()



## Helper fxn to convert variant to hgvs format (needed for queries)
def extractVariantID(row):
	chr = str(row['CHR'])
	position = str(row['POS'])
	ref = row['REF']
	alt = row['ALT']
	return "chr" + chr + ":g." + position + ref + ">" + alt

## Helper fxn to convert variant into query string
def chrPositionID(row):
	chr = str(row['CHR'])
	position_1 = int(row['POS'])
	position_2 = position_1 + 1
	return chr + "[chr] AND " + str(position_1) + ":" + str(position_2) + "[chrpos37]"

## Helper fxn that maps the variant's hgvs ID to its rsID
def rsID(row):
	varID = str(row['hgvs_id'])
	var = mv.getvariant(varID, fields='dbsnp.rsid')
	if 'dbsnp' in var:
		rsid =  var['dbsnp']['rsid']
	else:
		rsid = "none"
	return rsid


def convertVariantFile(psp_var_file, outfile):
	## load input file
	full_variant_DF = pd.read_excel(psp_var_file)
	
	## extract & rename subset of columns
	variant_DF = full_variant_DF[['VAR', 'REF', 'ALT', 'SNPEFF_GENE_NAME']]
	variant_DF.columns = ['VAR', 'REF', 'ALT', 'GENE_NAME']
	
	## convert variant ID format
	variant_DF['hgvs_id'] = full_variant_DF.apply(partial(extractVariantID), axis=1)
	
	## convert variant to corresponding query string
	variant_DF['variant_pos'] = full_variant_DF.apply(partial(chrPositionID), axis=1)
	
	## add rsID (where applicable)
	variant_DF['rsID'] = variant_DF.apply(partial(rsID), axis=1)
	variant_DF.columns = ['Variant', 'REF', 'ALT', 'Gene', 'hgvs_id', 'variant_pos', 'rsID']
	
	## write output file
	variant_DF.to_csv(outfile, sep='\t', index=False, header=True)
	return variant_DF



def runProgram(rare_var_file, excl_var_file, rare_out_file, excl_out_file):
	## convert PSP variant files
	rare_DF = convertVariantFile(rare_var_file, rare_out_file)
	exclusive_DF = convertVariantFile(excl_var_file, excl_out_file)
	
	## extract genes for geneList file
	gene_DF = pd.concat(rare_DF['Gene'], exclusive_DF['Gene'])
	gene_DF.sort_values(by='Gene', ascending=True, inplace=True)
	gene_DF = gene_DF['Gene'].drop_duplicates()
	
	## write geneList file
	geneList_file = rare_out_file.rpartition('/')[0] + "PSP_rare_exclusive_geneList.txt"
	gene_DF.to_csv(geneList_file, sep='\t', index=False, header=False)



## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started python")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("--var_rare",
						dest="psp_var_file_extremely_rare", type=valid_file, action='store',
						help="path for the PSP extremely rare variant file")
	parser.add_argument("--var_exclusive",
						dest="psp_var_file_exclusive", type=valid_file, action='store',
						help="path for the PSP exclusive variant file")
	
	parser.add_argument("--out_rare",
						dest="output_file_rare", type=str, action='store',
						help="Name for the Extremely rare output file")
	parser.add_argument("--out_exclusive",
						dest="output_file_exclusive", type=str, action='store',
						help="Name for the exclusive output file")
	
	pargs = parser.parse_args()
	
	psp_var_rare = pargs.psp_var_file_extremely_rare
	out_rare = pargs.output_file_rare
	
	psp_var_exclusive = pargs.psp_var_file_extremely_rare
	out_exclusive = pargs.output_file_exclusive
	
	runProgram(psp_var_rare, psp_var_exclusive, out_rare, out_exclusive)
	
	print("Finished")


