#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/01_convert_PSP_variant_format

PSP_VAR_FILE=$BASE_PATH/data_files_recd/orig_variant_lists_Jacky/PSP_exclusive_recurring_1021.xlsx
OUT_FILE=$BASE_PATH/PSP_rare_exclusive/PSP_exclusive_recurring_1021-varID.xlsx

python $SRC_PATH/psp_convert_Rare_variants.py -p $PSP_VAR_FILE  -o $OUT_FILE

