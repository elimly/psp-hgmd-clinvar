#!/bin/bash
echo "Bash driver script started.."

BASE_PATH=~/Dropbox/*Wang_Lab/PSP
SRC_PATH=~/repos/bitbucket/psp-hgmd-clinvar/01_convert_PSP_variant_format

GENE_LIST=$BASE_PATH/PD_genes/PD_genes.txt
PSP_VAR_FILE=$BASE_PATH/data_files_recd/orig_variant_lists_Jacky/psp_ND_gene_bigtable.0105.xlsx
OUT_FILE=$BASE_PATH/PD_genes/PSP_variants_PD_genes-LRRK2_012717.xlsx


python $SRC_PATH/psp_convert_ND_variants.py -p $PSP_VAR_FILE -g $GENE_LIST -o $OUT_FILE

