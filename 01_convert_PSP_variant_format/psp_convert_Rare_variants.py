#! /usr/bin/env python
from functools import partial
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## ClinVar query API - myvariant - setup
import myvariant
mv = myvariant.MyVariantInfo()



## Helper fxn to convert variant to hgvs format (needed for queries)
def extractVariantID(row):
	chr = str(row['CHR'])
	position = str(row['POS'])
	ref = row['REF']
	alt = row['ALT']
	return "chr" + chr + ":g." + position + ref + ">" + alt


## Helper fxn that maps the variant's hgvs ID to its rsID
def rsID(row):
	varID = str(row['var_ID'])
	var = mv.getvariant(varID, fields='dbsnp.rsid')
	if 'dbsnp' in var:
		rsid =  var['dbsnp']['rsid']
	else:
		rsid = "none"
	return rsid



def extractVarIDsubTable(varDF, outfile):
	variant_DF = varDF[['VAR', 'REF', 'ALT', 'SNPEFF_GENE_NAME', 'var_ID', 'rsID']]
	variant_DF.columns = ['Variant', 'REF', 'ALT', 'Gene', 'var_ID', 'rsID']
	
	## write output file
	variant_DF.to_csv(outfile, sep='\t', index=False, header=True)
	return variant_DF



def convertVariantFile(psp_var_file, outfile):
	## load input file
	variant_DF = pd.read_excel(psp_var_file)
	
	## convert variant ID format
	variant_DF['var_ID'] = variant_DF.apply(partial(extractVariantID), axis=1)
	
	## add rsID (where applicable)
	variant_DF['rsID'] = variant_DF.apply(partial(rsID), axis=1)
	
	## write output file
	variant_DF.to_excel(outfile, sheet_name='Orig', index=False, header=True)



## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started python")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--psp_variant",
						dest="psp_var_file", type=valid_file, action='store',
						help="path for the PSP variant file")
	parser.add_argument("-o", "--output",
						dest="output_file", type=str, action='store',
						help="Name for the resulting output file")
	pargs = parser.parse_args()
	
	psp_var_file = pargs.psp_var_file
	output_file = pargs.output_file
	
	convertVariantFile(psp_var_file, output_file)
	
	print("Finished")


