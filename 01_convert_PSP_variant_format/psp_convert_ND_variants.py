#! /usr/bin/env python
from functools import partial
import argparse, os
import pandas as pd
pd.options.mode.chained_assignment = None

## ClinVar query API - myvariant - setup
import myvariant
mv = myvariant.MyVariantInfo()



## Helper fxn to convert variant to hgvs format (needed for queries)
def extractVariantID(row):
	chr = str(row['CHR'])
	position = str(row['POS'])
	ref = row['REF']
	alt = row['ALT']
	return "chr" + chr + ":g." + position + ref + ">" + alt


## Helper fxn that maps the variant's hgvs ID to its rsID
def rsID(row):
	varID = str(row['var_ID'])
	var = mv.getvariant(varID, fields='dbsnp.rsid')
	if 'dbsnp' in var:
		rsid =  var['dbsnp']['rsid']
	else:
		rsid = "none"
	return rsid



def convertVariantFile(psp_var_file, gene_file, outfile):
	## load input files
	var_in_DF = pd.read_excel(psp_var_file)
	geneList = [line.rstrip('\n') for line in open(gene_file)]

	## select subset of genes
	var_in_DF = var_in_DF[var_in_DF['SNPEFF_GENE_NAME'].isin(geneList)]
	
	## convert variant ID format
	var_in_DF['var_ID'] = var_in_DF.apply(partial(extractVariantID), axis=1)
	
	## add rsID (where applicable)
	var_in_DF['rsID'] = var_in_DF.apply(partial(rsID), axis=1)
	
	## calculate Odds Ratios vs ADSP, ADGC & EXAc controls
	var_in_DF['OR [ADSP MAF]'] = var_in_DF['MAF'] / var_in_DF['MAF_ADSP']
	var_in_DF['OR [ADGC MAF]'] = var_in_DF['MAF'] / var_in_DF['ADGC MAF']
	var_in_DF['OR [EXAC MAF]'] = var_in_DF['MAF'] / var_in_DF['EXAC MAF']
	var_in_DF['OR [all MAF]'] = var_in_DF['MAF'] / var_in_DF['All_maf']

	## write output file
	var_in_DF.to_excel(outfile, sheet_name='Orig', index=False, header=True)




## Argparse helper fxn
def valid_file(x):
	if not os.path.exists(x):
		raise argparse.ArgumentTypeError("{0} does not exist".format(x))
	return x


if __name__ == "__main__":
	print("Started")
	
	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--psp_variant",
						dest="psp_var_file", type=valid_file, action='store',
						help="path for the PSP variant file")
	parser.add_argument("-g", "--gene_list",
						dest="gene_file", type=valid_file, action='store',
						help="path for the gene list file")
	parser.add_argument("-o", "--output",
						dest="output_file", type=str, action='store',
						help="Name for the resulting output file")
	pargs = parser.parse_args()
	
	psp_var_file = pargs.psp_var_file
	gene_file = pargs.gene_file
	output_file = pargs.output_file

	convertVariantFile(psp_var_file, gene_file, output_file)

	print("Program complete. goodbye")

